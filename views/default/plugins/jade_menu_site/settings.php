<?php

// get plugin
$plugin = elgg_extract("entity", $vars);

$yesno_options = array(
    "yes" => elgg_echo("option:yes"),
    "no" => elgg_echo("option:no")
);

if (!$plugin->affiche_menu_plus) {
    $plugin->affiche_menu_plus = 'yes';
}

echo "<div>";
echo '<label>' . elgg_echo("Afficher le menu more ?") . '</label>';
echo elgg_view("input/dropdown", array(
    "name" => "params[affiche_menu_plus]",
    "options_values" => $yesno_options,
    "value" => $plugin->affiche_menu_plus
));
echo '<p>' . elgg_echo("Afficher l'aide ?") . '</p>';
echo "</div>";

echo "<div>";
echo '<label>' . elgg_echo("test image") . '</label>';
echo elgg_view("input/text", array("name" => "params[logo_img]", "value" => $plugin->logo_img));
echo '<p>' . elgg_echo("image aide") . '</p>';
echo "</div>";

$menus = elgg()->menus->getUnpreparedMenu('site')->getItems();

$affiche = array(
    "yes" => elgg_echo("option:yes"),
    "no" => elgg_echo("option:no")
);

$priority_option = array("--");

for ($i = 1; $i <= count($menus); $i++){
     
    $priority_option[$i] = $i;
    
}

elgg_dump($menus);

foreach ($menus as $menu) {

    $name = $menu->getName();
    $formulaire = $name;
    $formulaire .= elgg_view('input/dropdown', array(
        "name" => "params[affiche_$name]",
        "options_values" => $affiche,
        "value" => $plugin->{"affiche_$name"}));
        
    $formulaire .= elgg_view('input/dropdown', [
        'name' => "params[priority_$name]",
        "options_values" => $priority_option,
        'value' => $plugin->{"priority_$name"}]);
        
        echo $formulaire;
}


